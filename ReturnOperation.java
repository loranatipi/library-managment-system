package edu.seeu.Library;

import java.util.Scanner;

public class ReturnOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("Return books");
        Scanner scanner=new Scanner(System.in);
        System.out.println("Please enter the name of the book to return:");
        String name=scanner.nextLine();
        for(int i=0;i< bookList.getUsedSize();i++){
            Book book=bookList.getBook(i);
            if(bookList.getName().equals(name)){
                bookList.setBorrowed(false);
                System.out.println("Return succeeded!");
                return;
            }
        }
        System.out.println("There are no books you want to return");
    }
}