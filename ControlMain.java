package edu.seeu.Library;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

import static antlr.build.ANTLR.root;

public class ControlMain {
    public static void main(String[] args) {
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/LibraryManagmentSystem","root","");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from LibraryManagmentSystem");
            while (resultSet.next()) {
                System.out.println(resultSet.getString("name"));
            }
        }catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
