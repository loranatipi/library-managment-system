package edu.seeu.Library;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.persistence.*;

@Entity
@Table(name="LibraryManagmentSystem")
public class Book extends Throwable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ISBN")
    private int ISBN;

    @Column(name = "name")
    private String name;
    @Column(name = "author")
    private String author;

    @Column(name = "price")
    private  int price;

    @Column(name = "type")
    private String type;
    private boolean isBorrowed;

    public Book() {
        name = "Unknown";
        author = "Unknown";
        price = 0;
        type = "Unknown";
        isBorrowed = false;
        ISBN =0;
    }

    public Book(String name, String author, int price, String type, int ISBN) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.type = type;
        this.ISBN = ISBN;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getAuthor() {
        return author;
    }
    public void setAuthor(String author) {
        this.author = author;
    }
    public int getPrice() {
        return price;
    }
    public void setPrice(int price) {
        if (price<0)
        {
            System.exit(0);
        }
        else
        this.price = price;
    }
    public String getType() {
        return type;
    }
    public void setType(String type) {
        this.type = type;
    }
    public boolean isBorrowed() {
        return isBorrowed;
    }
    public void setBorrowed(boolean borrowed) {
        isBorrowed = borrowed;
    }

    public int getISBN() {
        return ISBN;
    }

    public void setISBN(int ISBN) {
        this.ISBN = ISBN;
    }

    @Override
    public String toString() {
        return "Book{" +
                "name='" + name + '\'' +
                ", auther='" + author + '\'' +
                ", price=" + price +
                ", type='" + type + '\'' +
                ", isBorrowed=" + isBorrowed +
                ", ISBN=" + ISBN +
                '}';
    }
}
