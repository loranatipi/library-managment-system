package edu.seeu.Library;

import java.util.Scanner;

public class BorrowOperation extends Book implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("Borrow books");
        Scanner scanner=new Scanner(System.in);
        System.out.println("Please enter the name of the book to borrow:");
        String name=scanner.nextLine();

        for(int i=0;i< bookList.getUsedSize();i++){
            Book book=bookList.getBook(i);
            if(book.getClass().equals(name)){
                bookList.setBorrowed(true);
                System.out.println("Borrowing succeeded!");
                return;
            }
            System.out.println("There is no book you want to borrow");
        }
    }
}