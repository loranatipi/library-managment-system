package edu.seeu.Library;

public interface IOperation {
    void work(BookList bookList);

}