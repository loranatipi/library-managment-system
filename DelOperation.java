package edu.seeu.Library;

import java.util.Scanner;

public class DelOperation extends Throwable implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("Delete book");
        Scanner scanner=new Scanner(System.in);
        System.out.println("Please enter the name of the book you want to delete:");
        String name=scanner.nextLine();
        int i=1;
        for(;i<= bookList.getUsedSize();i++){
            String book=bookList.getName(i);
            if(bookList.getName().equals(name)){
                break;
            }
            else
            {
                System.out.println("There is no book to delete");
                System.exit(0);
            }
        }
        while (i==bookList.getUsedSize()){
            System.out.println("There is no book!");
            break;
        }
        for(int pos=i;pos<bookList.getUsedSize()-1;pos++){
            Book book= bookList.getBook(pos+1);
            bookList.setBooks(pos,book);
        }
        bookList.setUsedSize(bookList.getUsedSize()-1);
        System.out.println("Delete succeeded!");
    }
}