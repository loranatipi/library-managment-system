package edu.seeu.Library;

public abstract class User {
    protected String name;
    protected IOperation[] operations;
    public User(String name){
        this.name=name;
    }
    public abstract int menu();
    public void doOperation(BookList bookList, int choice){
        this.operations[choice-1].work(bookList);
    }

}