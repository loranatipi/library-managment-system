package edu.seeu.Library;

public class DisplayOperation implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("Show books");
        for(int i=0;i<bookList.getUsedSize();i++){
            Book book=bookList.getBook(i);
            System.out.println(book);
        }
    }
}