package edu.seeu.Library;

import java.util.Scanner;

public class AddOperation extends Throwable implements IOperation{
    @Override
    public void work(BookList bookList) {
        System.out.println("New books");

        Scanner scanner=new Scanner(System.in);
        System.out.println("Please enter the name of the book:");
        String name=scanner.nextLine();
        System.out.println("Please enter the author of the book:");
        String author=scanner.nextLine();
        System.out.println("Please enter the price of the book:");
        int price=scanner.nextInt();
        if (price<0)
        {
            System.out.println("Price cannot be less then 0!");
            System.exit(0);
        }
        System.out.println("Please enter the type of book");
        String type=scanner.next();
        System.out.println("Please enter the ISBN of the book");
        int isbn=scanner.nextInt();

        Book book=new Book(name,author,price,type, isbn);
        int curSize=bookList.getUsedSize();
        bookList.setBooks(curSize,book);
        bookList.setUsedSize(curSize+1);
        System.out.println("Successfully added!");

        
    }


}