package edu.seeu.Library;

import java.util.Scanner;

public class Admin extends User {
    public Admin(String name){
        super(name);
        this.operations=new IOperation[]{
                new FindOperation(),
                new AddOperation(),
                new DelOperation(),
                new DisplayOperation()
        };
    }
    @Override
    public int  menu() {
        System.out.println("================");
        System.out.println("hello"+" "+this.name+" "+"Welcome to the library management system");
        System.out.println("1.Find books");
        System.out.println("2.New book");
        System.out.println("3.Delete book");
        System.out.println("4.Show all books");
        System.out.println("================");
        System.out.println("Please enter your choice:");
        //Enter data after seeing the menu
        Scanner scanner=new Scanner(System.in);
        int choice=scanner.nextInt();
        return choice;//Returns the selected value
    }
}