package edu.seeu.Library;

import java.util.Scanner;

public class FindOperation implements IOperation {
    @Override
    public void work(BookList bookList) {
        System.out.println("Find books");
        Scanner scanner=new Scanner(System.in);
        System.out.println("Please enter the name of the book to find:");
        String name=scanner.nextLine();
        for(int i=0;i< bookList.getUsedSize();i++){
            Book book=bookList.getBook(i);
            if(bookList.getName().equals(name)){
                System.out.println(book);
                System.out.println("Search succeeded!");
                return;
            }
        }
        System.out.println("There is no book you are looking for");
    }
}