package edu.seeu.Library;

import java.util.ArrayList;
import java.util.List;

public class BookList extends Book{

    private List<String> books=new ArrayList<>();

    public BookList(List<String> books) {
        this.books = books;
    }

    public BookList(String name, String author, int price, String type, int ISBN, int usedSize) {
        super(name, author, price, type, ISBN);
        this.usedSize = usedSize;
    }

    public BookList(int usedSize) {
        this.usedSize = usedSize;
    }
    public BookList(String name, String author, int price, String type, int ISBN, List<String> books) {
        super(name, author, price, type, ISBN);
        this.books = books;
    }

    public List<String> getBooks() {
        return books;
    }

    public void setBooks(List<String> books) {
        this.books = books;
    }

    private int usedSize = 0;

    public BookList() {

    }


    public void setBooks(int pos, Book book) {
        this.books = (List<String>) books;
    }

    public Book getBook(int pos) {
        return this.getBook(pos);
    }

    public int getUsedSize() {
        return usedSize;
    }

    public void setUsedSize(int usedSize) {
        if(getUsedSize()<0)
        {
            System.out.println("Cannot be size less than 0");
            System.exit(0);
        }
        else
            this.usedSize = usedSize;
    }
    public String getName(int n) {
        return getName();
    }

}