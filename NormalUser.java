package edu.seeu.Library;

import java.util.Scanner;

public class NormalUser extends User{

    public NormalUser(String name) {
        super(name);
        this.operations=new IOperation[]{
                new FindOperation(),
                new BorrowOperation(),
                new ReturnOperation()

        };
    }
    @Override
    public int menu() {
        System.out.println("================");
        System.out.println("hello"+" "+this.name+" "+"Welcome to the library management system");
        System.out.println("1.Find books");
        System.out.println("2.Borrow books");
        System.out.println("3.Return books");
        System.out.println("================");
        System.out.println("Please enter your choice:");
        //Enter data after seeing the menu
        Scanner scanner=new Scanner(System.in);
        int choice=scanner.nextInt();
        return choice;
    }
}